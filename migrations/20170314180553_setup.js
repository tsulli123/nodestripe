
exports.up = (knex, Promise) => {
    return Promise.all([
        knex.schema.createTableIfNotExists('profile', (t) => {
            t.increments('id').primary();
            t.string('first_name');
            t.string('last_name');
            t.string('email');
            t.string('image');
            t.date('dob');
            t.string('stripe', 60);
            t.boolean('verified').defaultTo(false);
        }),
        knex.schema.createTableIfNotExists('wedding', (t) => {
            t.increments('id').primary();
            t.string('stripe', 60);
            t.string('image');
            t.date('event');
            t.integer('profile_id').unsigned();
            t.foreign('profile_id').references('profile.id');
            t.integer('partner_id').unsigned();
            t.foreign('partner_id').references('profile.id');
        }),
        knex.schema.createTableIfNotExists('wedding_profile', (t) => {
            t.increments('id').primary();
            t.integer('profile_id').unsigned();
            t.foreign('profile_id').references('profile.id');
            t.integer('wedding_id').unsigned();
            t.foreign('wedding_id').references('wedding.id');
        }),
        knex.schema.createTableIfNotExists('wedding_donation', (t) => {
            t.increments('id').primary();
            t.string('email');
            t.string('receipt');
            t.timestamp('message');
            t.string('image');
            t.decimal('amount');
            t.integer('postcode');
            t.integer('wedding_id').unsigned();
            t.foreign('wedding_id').references('wedding.id');
        }),
        knex.schema.createTableIfNotExists('email_login', (t) => {
            t.increments('id').primary();
            t.string('password').notNullable();
            t.string('api_key', 64);
            t.string('api_expiry');
            t.string('api_token');
            t.string('verify_key', 64).notNullable();
            t.boolean('verified').defaultTo(false);
            t.integer('profile_id').unsigned();
            t.foreign('profile_id').references('profile.id');
            t.timestamps();
        })
    ]);
};


exports.down = (knex, Promise) => {
    return Promise.all([
        knex.schema.dropTableIfExists('wedding_donation'),
        knex.schema.dropTableIfExists('wedding_profile'),
        knex.schema.dropTableIfExists('wedding'),
        knex.schema.dropTableIfExists('email_login'),
        knex.schema.dropTableIfExists('profile')
    ]);
};
