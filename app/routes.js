module.exports = function (app, stripe, controller, fs, imageData) {

	var paths = {
		buildFiles: {
			root: './public'
		}
	}


	//load admin routes
	//require('./routes/admin.js')(app, passport, validator, paths);


	//main routing

	//landing page
	app.get('/', function (req, res) {
		res.sendFile('test.html', paths.buildFiles);
	});

	app.get('/iww/test', function (req, res) {

		console.log(req.query);
		res.send('ok');

	});

	app.get('/iww/api/photo', function (req, res) {
		//console.log(req.query);

		var fileName = req.query.groomFN + "_" + req.query.brideFN + ".jpg";

		var filePath = './images/' + fileName;

		// Returns a Promise
		imageData.outputFile(req.query.image, filePath).then(function () {

			var fp = fs.readFileSync(filePath);
			console.log(filePath);
			console.log(fp);
			stripe.fileUploads.create({
					purpose: 'identity_document',
					file: {
						data: fp,
						name: fileName,
						type: 'application/octet-stream'
					}

				}),
				function (err, fileUpload) {
					console.log(err);
					console.log(fileUpload);
					res.send(fileName);
				};


		})


		// RETURNS image path of the created file 'out/path/fileName.png'



		/*fs.readFile(req.files.displayImage.path, function (err, data) {
			// ...
			var newPath = __dirname + "/uploads/uploadedFileName";
			fs.writeFile(newPath, data, function (err) {
				res.redirect("back");
			});
		});*/
	});

	app.get('/iww/createAcc', function (req, res) {

		console.log("");
		console.log(req.ip);
		console.log(req.connection.remoteAddress);
		console.log(req.query);

		stripe.accounts.create({
			managed: true,
			country: 'AU',
			email: req.query.email,
			external_account: req.query.btok,
			transfer_schedule: {
				delay_days: 14,
				interval: "daily"
			},
			transfer_statement_descriptor: "Wedding Wishes Amount",
			tos_acceptance: {
				date: Math.floor(Date.now() / 1000),
				ip: req.ip
			},
			legal_entity: {
				dob: {
					day: req.query.day,
					month: req.query.month,
					year: req.query.year
				},
				first_name: req.query.groomFN,
				last_name: req.query.groomLN,
				type: "individual",
				address: {
					city: req.query.city,
					line1: req.query.add1,
					postal_code: req.query.zip,
					state: req.query.state
				}
			}
		}, function (err, account) {
			// asynchronously called
			console.log("err:");
			console.log(err);
			console.log("account:");
			console.log(account)

			if (account) {
				console.log(account.id);
				var accountID = account.id;
				var sk = account.keys.secret;
				var fileName = req.query.groomFN + "_" + req.query.brideFN + ".jpg";

				var filePath = './images/' + fileName;

				// Returns a Promise
				imageData.outputFile(req.query.image, filePath).then(function () {

					var fp = fs.readFileSync(filePath);
					//console.log(filePath);
					//console.log(fp);

					stripe.fileUploads.create({
							purpose: 'identity_document',
							file: {
								data: fp,
								name: fileName,
								type: 'application/octet-stream'
							}

						},
						function (err, fileUpload) {
							console.log(err);
							console.log(fileUpload);
							var subStripe = require('stripe')(sk);
							console.log(accountID);
							stripe.accounts.update(
								accountID, {
									legal_entity: {
										verification: {
											document: fileUpload.id
										}
									}
								});
							//res.send('image has been uploaded');
						});


				});

				var weddingData = {

					email: req.query.email,
					stripeId: account.id,
					secretKey: account.keys.secret,
					publishKey: account.keys.publishable,
					groomFName: req.query.groomFN,
					groomLName: req.query.groomLN,
					brideFName: req.query.brideFN,
					brideLName: req.query.brideLN,
					date: req.query.date
				}

				controller.Wedding.createWedding(weddingData, function (wedding) {
					console.log("wedding created");
					console.log(wedding);

					res.send({
						img: req.query.image,
						id: wedding._id,
						gfn: req.query.groomFN,
						bfn: req.query.brideFN,
						wid: makeid()
					})
				});

				//save account id and keys -> database. (including email and names)
				// create account bank or card details -> choose today.
				/*stripe.customers.createSource(
	account.id, {
		source: req.query.btok
	},
	function (err, bank) {
		// asynchronously called

		console.log(bank);
		console.log(err);

		controller.Wedding.createWedding(weddingData, function (wedding) {
			console.log("wedding created");
			console.log(wedding);
			res.send("your account has been created");
		});

	}
);*/


			} else {

				res.send("Your Wishing Well Account could not be created");

			}
		});

	});

	app.get('/iww/giftAcc', function (req, res) {

		//search accoundID by email req.query.accEmail, make async call to send account.id into stripe_account section
		controller.Wedding.findById(req.query.id, function (account) {
			console.log(account);
			stripe.charges.create({
					amount: req.query.wishAmount * 100, // amount in cents
					currency: "aud",
					source: req.query.token,
					description: "Example charge",
					receipt_email: req.query.email,
					application_fee: parseInt((req.query.wishAmount * 100) * 0.017) // amount in cents

				}, {
					stripe_account: account.stripeId
				},
				function (err, charge) {
					// check for `err`
					// do something with `charge`
					console.log("charge:");
					console.log(charge);
					console.log("err:");
					console.log(err);

					if (charge) {

						res.send("Your Wish has been sent, have a good day at the wedding!");

					} else res.send("An Error has occured, please check your details properly.")
				}
			);
		});

	});

	app.get('/iww/invoiceClient', function (req, res) {});

	app.get('/www/deleteAcc', function (req, res) {});


	function makeid() {
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		for (var i = 0; i < 5; i++)
			text += possible.charAt(Math.floor(Math.random() * possible.length));

		return text;
	}

};