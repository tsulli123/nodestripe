module.exports = {
	email: {
		service: 'gmail',
		//host: 'smtp.gmail.com',
		auth: {
			user: 'iwishingwells@gmail.com',
			pass: 'GoogleAccount1'
		}
	},
	knex: {
		client: 'mysql',
		connection: {
			host: 'localhost',
			port: '3306',
			database: 'iwishingwell',
			user: 'root',
			password: ''
		}
	},
	database: {
		host: 'localhost',
		port: '3306',
		database: 'iwishingwell',
		user: 'root',
		password: ''
	},
	session: {
		host: 'localhost',
		port: '3306',
		database: 'iwishingwell',
		user: 'root',
		password: '',
		checkExpirationInterval: 900000, // How frequently expired sessions will be cleared; milliseconds.
		expiration: 86400000, // The maximum age of a valid session; milliseconds.
		createDatabaseTable: true, // Whether or not to create the sessions database table, if one does not already exist.
		connectionLimit: 1, // Number of connections when creating a connection pool
		schema: {
			tableName: 'login_sessions',
			columnNames: {
				session_id: 'session_id',
				expires: 'expires',
				data: 'data'
			}
		}
	}
};