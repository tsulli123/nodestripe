/*
 * Client Schema
 * client data
 */
module.exports = function (mongoose, schemas) {

	schemas['Wedding'] = mongoose.Schema({
		email: String,
		stripeId: String,
		secretKey: String,
		publishKey: String,
		groomFName: String,
		groomLName: String,
		brideFName: String,
		brideLName: String,
		date: Date
	});
	return mongoose.model('Wedding', schemas.Wedding);
};