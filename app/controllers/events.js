
module.exports = () => {
    var events = [];
    return {
        on: function OnEvent(id, callback) {
            if (!events.hasOwnProperty(id)) events[id] = [];
            events[id].push(callback);
            return this;
        },
        fire: function FireEvent(id, data) {
            if (events.hasOwnProperty(id)) for (var i in events[id]) events[id][i](data);
            return this;
        }
    };
};
