module.exports = knex => {

    var weddings = (phrase) => {
        return new Promise((resolve, reject) => {
            var values = phrase.split(' ');
            knex.select(
	                'w.*',
	                'pro.first_name AS profile_first_name',
					'pro.last_name AS profile_last_name',
	                'par.first_name AS partner_first_name',
					'par.last_name AS partner_last_name'
	            )
	            .from(['wedding AS w'])
	            .join('profile AS pro', 'w.profile_id', '=', 'pro.id')
	            .join('profile AS par', 'w.partner_id', '=', 'par.id')
                .whereIn('pro.first_name', values)
                .orWhereIn('pro.last_name', values)
                .orWhereIn('par.first_name', values)
                .orWhereIn('par.last_name', values)
	            .then((res) => {
	                resolve(res);
	            }).catch((err) => {
	                reject(err);
	            });
        });
    };

    return {
        weddings: weddings
    };
};
