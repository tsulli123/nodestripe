
module.exports = (passport, knex, LocalStrategy, jwt) => {

    var crypto = require('crypto');

    var find = function Find(key, value) {
        return new Promise((resolve, reject) => {
            knex
                .select('l.*', 'p.email AS email', 'p.first_name', 'p.last_name', 'p.id AS pid', 'p.stripe AS stripe')
                .from('email_login AS l')
                .where(key, value)
                .join('profile AS p', 'profile_id', '=', 'p.id')
                .then(res => {
                    resolve(res[0]);
                }).catch(err => {
                    reject(err);
                });
        });
    };

    var getProfile = function GetProfile(id) {
        return new Promise((resolve, reject) => {
            knex
                .select('l.id', 'l.profile_id', 'p.email AS email', 'p.first_name', 'p.last_name', 'p.id AS pid', 'p.stripe AS stripe')
                .from('email_login AS l')
                .where('id', id)
                .join('profile AS p', 'profile_id', '=', 'p.id')
                .then(res => {
                    resolve(res[0]);
                }).catch(err => {
                    reject(err);
                });
        });
    };

    passport.serializeUser((user, done) => {
        done(null, user.id);
    });

    passport.deserializeUser((id, done) => {
        find('l.id', id).then(user => {
            if (user) done(null, user);
            else done('Login attempt failed!', null);
        }, err => {
            done('Login attempt failed!', null);
        });
    });

    function LocalLogin(email, password) {
        return new Promise((resolve, reject) => {
            find('email', email).then(user => {
                if (!user) reject('Invalid username or password!');
                if (user.password === password) resolve(user);
                else reject('Invalid username or password!');
            }, reject);
        });
    }

    passport.use(new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true
    }, (req, email, password, done) => {
        LocalLogin(email, password).then(user => {
            return done(null, user);
        }).catch(reason => {
            return done(null, false, req.flash('error', 'Login attempt failed!'));
        });
    }));

    var create = function Create(user) {
        return new Promise((resolve, reject) => {
            knex.transaction(trx => {
                return trx
                    .insert({
                        email: user.email,
                        first_name: user.firstname,
                        last_name: user.lastname
                    })
                    .into('profile')
                    .then(ids => {
                        return trx.insert({
                            password: user.password,
                            verify_key: user.key,
                            profile_id: ids[0]
                        })
                        .into('email_login')
                        .then(pid => {
                            resolve([ids[0], pid[0]]);
                        });
                    });
            }).catch(err => {
                reject(err);
            });
        });
    };




    var verify = function Verify(id, key) {
        return new Promise((resolve, reject) => {
            knex.distinct(1)
                .select('id', 'verified', 'verify_key')
                .from('email_login')
                .where('id', id)
                .where('verify_key', key)
                .then(res => {
                    if (res[0]) return knex('email_login')
                        .where('id', parseInt(res[0].id))
                        .update('verified', true)
                        .then(result => {
                            resolve(result);
                        });
                    else reject(false);
                }).catch(err => {
                    reject(err);
                });
        });
    };





    var update = function Update(id, password) {
        return new Promise((resolve, reject) => {
            knex('email_login')
                .where('id', id)
                .update('password', password)
                .then((res) => {
                    resolve(res);
                }).catch((err) => {
                    reject(err);
                });
        });
    };





    var generateApiKey = function GenerateApiKey(id) {
        return new Promise((resolve, reject) => {
            var key = crypto.randomBytes(12).toString('hex');
            var token = jwt.sign({id: id}, key);
            var data = {
                api_token: token,
                api_key: key,
                api_expiry: new Date().toString()
            };
            knex('email_login')
                .where('id', id)
                .update(data)
                .then((res) => {
                    resolve(data);
                }).catch((err) => {
                    reject(err);
                });
        });
    };

    var localApiLogin = function LocalApiLogin(email, password) {
        return new Promise((resolve, reject) => {
            find('email', email).then(user => {
                if (!user) reject('Invalid username or password!');
                if (user.password === password) {
                    generateApiKey(user.id).then(res => {
                        resolve(res);
                    }).catch(err => {
                        reject('Api key generation failed!');
                    });
                }
                else reject('Invalid username or password!');
            }, reject);
        });
    }

    var verifyApiKey = function VerifyApiKey(token) {
        return new Promise((resolve, reject) => {
            knex.select('*')
                .from('email_login')
                .where('api_token', token)
                .then(res => {
                    resolve(res[0]);
                }).catch(err => {
                    reject(err);
                });
        });
    }

    return {
        create: create,
        find: find,
        verify: verify,
        apiLogin: localApiLogin,
        verifyApiKey: verifyApiKey,
        getProfile: getProfile
    };

};
