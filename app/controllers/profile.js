
module.exports = function (knex) {

    var stripe = function Stripe(id, stripeId) {
        return new Promise((resolve, reject) => {
            knex('profile')
                .where('id', id)
                .update('stripe', stripeId)
                .then((res) => {
                    resolve(res);
                }).catch((err) => {
                    reject(err);
                });
        });
    };

	return {
		stripe: stripe
	};
};
