module.exports = function (knex) {

	var find = function Find(key, value) {
		return new Promise((resolve, reject) => {
			knex.select(
					'w.*',
					'pro.first_name AS profile_first_name',
					'pro.last_name AS profile_last_name',
					'pro.email AS profile_email',
					'par.first_name AS partner_first_name',
					'par.last_name AS partner_last_name',
					'par.email AS partner_email'
				)
				.from(['wedding AS w'])
				.where('w.' + key, value)
				.join('profile AS pro', 'w.profile_id', '=', 'pro.id')
				.join('profile AS par', 'w.partner_id', '=', 'par.id')
				.then((res) => {
					resolve(res[0]);
				}).catch((err) => {
					reject(err);
				});
		});
	};

	var create = function Create(wedding) {
		return new Promise((resolve, reject) => {
			knex.transaction((trx) => {
				return trx
					.insert({
						email: wedding.email,
						first_name: wedding.firstname,
						last_name: wedding.lastname
					})
					.into('profile')
					.then((ids) => {
						return trx
							.insert({
								event: wedding.event,
								image: wedding.image,
								profile_id: wedding.profile,
								partner_id: ids[0]
							})
							.into('wedding');
					});
			}).then((res) => {
				resolve(res[0]);
			}).catch((err) => {
				reject(err);
			});
		});
	};

	var donate = function Donate(donation) {
		return new Promise((resolve, reject) => {
			knex.transaction((trx) => {
				return trx
					.insert({
						wedding_id: donation.wedding,
						email: donation.email,
						message: donation.message,
						postcode: donation.postcode,
						amount: donation.amount
					})
					.into('wedding_donation')
			}).then((res) => {
				resolve(res[0]);
			}).catch((err) => {
				reject(err);
			});
		});
	};

	var donations = function Donations(id) {
		return new Promise((resolve, reject) => {
			knex.select('*')
				.from('wedding_donation')
				.where('wedding_id', id)
				.then((res) => {
					resolve(res);
				}).catch((err) => {
					reject(err);
				});
		});
	};

	var stripe = function Stripe(id, stripeId) {
		return new Promise((resolve, reject) => {
			knex('wedding')
				.where('id', id)
				.update('stripe', stripeId)
				.then((res) => {
					resolve(res);
				}).catch((err) => {
					reject(err);
				});
		});
	};

	return {
		find: find,
		create: create,
		stripe: stripe,
		donate: donate,
		donations: donations
	};
};