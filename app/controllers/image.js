
module.exports = (fs, imageData) => {

    var get = function Get(filename, imageDataUri) {
        return new Promise((resolve, reject) => {
            imageData.outputFile(imageDataUri, './images/' + filename).then((path) => {
                fs.readFile(path, (err, data) => {
                    if (data) resolve({
                        data: data,
                        path: path
                    });
                    else reject(err);
                });
            });
        });
    };

    return {
        get: get
    };
};
