module.exports = (stripe) => {

	var create = function Create(data) {
		console.log("")
		console.log("testing data going into stripe");
		console.log(data);
		return new Promise((resolve, reject) => {
			// change languages for global release
			stripe.accounts.create({
				managed: true,
				country: 'AU', // data.country,
				email: data.email,
				//external_account: data.btok,
				transfer_schedule: {
					delay_days: 2,
					interval: "daily"
				},
				transfer_statement_descriptor: "Wedding Wishes Amount",
				tos_acceptance: {
					date: Math.floor(Date.now() / 1000),
					ip: data.ip
				},
				legal_entity: {
					first_name: data.first_name,
					last_name: data.last_name,
					type: "individual"
				}
			}, (err, res) => {
				if (res) resolve(res);
				else reject(err);
			});
		});
	};

	var verify = function Verify(id, filename, filedata) {
		return new Promise((resolve, reject) => {
			stripe.fileUploads.create({
				purpose: 'identity_document',
				file: {
					data: filedata,
					name: filename,
					type: 'application/octet-stream'
				}
			}, (err, res) => {
				if (err) reject(err);
				else stripe.accounts.update(id, {
					legal_entity: {
						verification: {
							document: res.id
						}
					}
				}, (err, res) => {
					if (res) resolve(res);
					else reject(err);
				});
			});
		});
	};

	var createBank = function CreateBank(id, btok) {
		return new Promise((resolve, reject) => {
			stripe.accounts.createExternalAccount(id, {
				external_account: btok
			}, (err, res) => {
				if (res) resolve(res);
				else reject(err);
			})
		})
	};

	var update = function Update(id, data) {
		return new Promise((resolve, reject) => {
			stripe.accounts.update(id, data, (err, res) => {
				if (res) resolve(res);
				else reject(err);
			});
		});
	};

	var charge = function Charge(id, data) {
		return new Promise((resolve, reject) => {
			stripe.charges.create({
				amount: data.amount * 100, // amount in cents
				currency: "aud",
				source: data.token,
				description: "Wedding Wish from: " + data.name,
				receipt_email: data.email,
				application_fee: parseInt((data.amount * 100) * 0.012) // amount in cents
			}, {
				stripe_account: id
			}, (err, res) => {
				if (res) resolve(res);
				else reject(err);
			});
		});
	};

	return {
		createBank: createBank,
		create: create,
		verify: verify,
		update: update,
		charge: charge
	};
};
