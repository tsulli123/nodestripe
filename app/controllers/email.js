 module.exports = (nodemailer, config) => {

 	var transport = nodemailer.createTransport(config);

 	var send = (to, url, callback) => {
 		var message = {
 			from: 'iwishingwells@gmail.com',
 			to: to,
 			subject: 'iWishingWell - email validation',
 			text: 'iWishingWell \r\n\r\n  email validation link: \r\n' + url,
 			html: '<h1>iWishingWell - email validation</h1><p><a href="' + url + '">' + url + '</a></p>'
 		};
 		transport.sendMail(message, callback);
 	};


 	return {
 		send: send
 	};
 };