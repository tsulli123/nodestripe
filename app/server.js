//Server Connection Module

module.exports = (http, serverPort, theme) => {

	var server = http.listen(serverPort, () => {

		var info = {
			host: server.address().address,
			port: server.address().port
		};

		console.log('\t\t', theme('iWishingWell stripe payment server and admin \n'));
		console.log('\t\t', theme('Server listening on: ' + JSON.stringify(info)));
	});

	return server;
};
