module.exports = function (app, stripe, controller, fs, imageData) {

	return {

		/*
		 * create stripe account, without verification.
		 */
		createStripe: function (data, callback) {

			stripe.accounts.create({
				managed: true,
				country: data.country,
				email: data.email,
				//external_account: data.btok,
				transfer_schedule: {
					delay_days: 2,
					interval: "daily"
				},
				transfer_statement_descriptor: "Wedding Wishes Amount",
				tos_acceptance: {
					date: Math.floor(Date.now() / 1000),
					ip: data.ip
				}
			}, function (err, account) {
				// asynchronously called
				console.log("err:");
				console.log(err);
				console.log("account:");
				console.log(account)

				if (account) {

					//account creation success


					//save account id and keys -> database. (including email and names)
					// create account bank or card details -> choose today.
					/*stripe.customers.createSource(
	account.id, {
		source: req.query.btok
	},
	function (err, bank) {
		// asynchronously called

		console.log(bank);
		console.log(err);

		controller.Wedding.createWedding(weddingData, function (wedding) {
			console.log("wedding created");
			console.log(wedding);
			res.send("your account has been created");
		});

	}
);*/


				} else {
					//account creation error for stripe
					res.send("Your Wishing Well Account could not be created");

				}


			})
		},
		verifyImageStripe: function (data, callback) {

			console.log(data.id);
			var accountID = data.id;
			var sk = account.keys.secret;
			//need to save image name to database.
			var fileName = data.groomFN + "_" + data.brideFN + ".jpg";

			var filePath = './images/' + fileName;

			// Returns a Promise
			imageData.outputFile(data.image, filePath).then(function () {

				var fp = fs.readFileSync(filePath);
				//console.log(filePath);
				//console.log(fp);

				stripe.fileUploads.create({
						purpose: 'identity_document',
						file: {
							data: fp,
							name: fileName,
							type: 'application/octet-stream'
						}

					},
					function (err, fileUpload) {
						console.log(err);
						console.log(fileUpload);
						console.log(accountID);
						stripe.accounts.update(
							accountID, {
								legal_entity: {
									verification: {
										document: fileUpload.id
									}
								}
							});
						// if need a callback put here.
						//res.send('image has been uploaded');
					});

				/*var weddingData = {

    email: req.query.email,
    stripeId: account.id,
    secretKey: account.keys.secret,
    publishKey: account.keys.publishable,
    groomFName: req.query.groomFN,
    groomLName: req.query.groomLN,
    brideFName: req.query.brideFN,
    brideLName: req.query.brideLN,
    date: req.query.date
}

controller.Wedding.createWedding(weddingData, function (wedding) {
    console.log("wedding created");
    console.log(wedding);

    res.send({
        img: req.query.image,
        id: wedding._id,
        gfn: req.query.groomFN,
        bfn: req.query.brideFN,
        wid: makeid()
    })
});*/
			});

		},
		updateStripe: function (data, stripeID) {
			stripe.accounts.update(
				stripeID, {
					data
				});

			/*
					legal_entity: {
                    dob: {
                        day: data.day,
                        month: data.month,
                        year: data.year
                    },
                    first_name: data.groomFN,
                    last_name: data.groomLN,
                    type: "individual",
                    address: {
                        city: req.query.city,
                        line1: req.query.add1,
                        postal_code: req.query.zip,
                        state: req.query.state
                    }
                }
			*/
		},
		chargeAccount: function (stripeID, data, callback) {
			stripe.charges.create({
					amount: data.wishAmount * 100, // amount in cents
					currency: "aud",
					source: data.token,
					description: "Wedding Wish from: " + data.name,
					receipt_email: data.email,
					application_fee: parseInt((data.wishAmount * 100) * 0.012) // amount in cents

				}, {
					stripe_account: stripeID
				},
				function (err, charge) {
					// check for `err`
					// do something with `charge`
					console.log("charge:");
					console.log(charge);
					console.log("err:");
					console.log(err);

					if (charge) {

						//res.send("Your Wish has been sent, have a good day at the wedding!");

					} else {} //res.send("An Error has occured, please check your details properly.")
				}
			}
		}
	};
};