module.exports = (express, app, multer, controllers) => {

	var path = './images/wedding/';
	var storage = multer.diskStorage({
		destination: function (req, file, cb) {
			cb(null, path);
		},
		filename: (req, file, cb) => {
			cb(null, req.user.first_name + '_' + req.user.last_name + '_&_' + req.body.firstname + '_' + req.body.lastname + '.jpg');
		}
	});
	var uploads = multer({
		storage: storage,
		dest: path
	});


	controllers.event.on('wedding_created', (id) => {

	});

	function secure(req, res, next) {
		if (req.isAuthenticated()) {
			next();
		} else {
			res.redirect('/');
		}
	}

	var router = express.Router();

	router.get('/create', secure, (req, res) => {
		res.render('wedding_create', {
			error: req.flash('error')
		});
	});

	router.post('/create', secure, uploads.single('image'), (req, res) => {
		console.log(new Date(req.body.date));
		controllers.wedding.create({
			email: req.body.email,
			date: new Date(req.body.date),
			firstname: req.body.firstname,
			lastname: req.body.lastname,
			image: req.file.filename,
			profile: req.user.profile_id
		}).then((id) => {
			controllers.event.fire('wedding_created', id);
			res.redirect('/wedding/' + id + '/created');
		}, (err) => {
			req.flash('error', 'Wedding creation failed!')
			res.redirect('/wedding/create');
		});
	});



	router.get('/:id/created', secure, (req, res) => {
		var id = req.params.id;
		controllers.wedding.find('id', id).then((wedding) => {
			wedding.error = req.flash('error');
			res.render('wedding_created', wedding);
		}, (err) => {
			console.log('ERROR', err);
			req.flash('error', 'Wedding not found!')
			res.redirect('/wedding/' + id + '/account');
		});
	});

	router.get('/:id/account', (req, res) => {
		var id = req.params.id;
		controllers.wedding.find('id', id).then((wedding) => {
			wedding.error = req.flash('error');
			res.render('account', wedding);
		}, (err) => {
			console.log('ERROR', err);
			req.flash('error', 'Wedding not found!')
			res.redirect('/wedding/' + id + '/account');
		});
	});

	router.post('/:id/account', (req, res) => {
		console.log(req.user);
		console.log(req.body);

		var id = req.params.id;
		var filename = './wedding_images/' + id + '.jpg';


		controllers.stripe.createBank(req.user.stripe, req.body.idcode).then(data => {
			console.log(data);
			controllers.stripe.update(req.user.stripe, {
				legal_entity: {
					dob: {
						day: req.body.day,
						month: req.body.month,
						year: req.body.year
					},
					address: {
						city: req.body.city,
						line1: req.body.address1,
						postal_code: req.body.postcode,
						state: req.body.state
					}
				}
			}).then(data => {
				console.log(data)
				controllers.image.get(filename, req.body.image).then(image => {

					console.log(image);

					controllers.stripe.verify(req.user.stripe, image.path, image.data).then(response => {
						console.log("");
						console.log("CHECKING WHERE THIS IS");
						console.log(response);
						console.log("")

						controllers.wedding.stripe(id, req.user.stripe).then(ids => {
							console.log("is it getting in here?");
							res.send("ok");
						});
					});
				}, err => {
					res.send("Error with image upload.");
				});
			}, (err) => {
				console.log(err);
				res.send("Error with updating account");
			});
		}, (err) => {
			console.log(err);
			res.send("Error with Creating bank account.");
		});

	});


	router.get('/:id/donations', (req, res) => {
		var id = req.params.id;
		controllers.wedding.donations(id).then((donations) => {

			res.send(donations);

		}, (err) => {
			console.log('ERROR', err);

			res.render('wedding', {});
		});

	});


	router.get('/:id/donate', (req, res) => {
		var id = req.params.id;
		controllers.wedding.find('id', id).then((wedding) => {
			wedding.error = req.flash('error');
			res.render('donate', wedding);
		}, (err) => {
			res.render('donate', {
				error: "An error occured with your request"
			});
		});
	});

	router.post('/:id/donate', (req, res) => {
		var id = req.params.id;
		var donation = {
			wedding: id,
			email: req.body.email,
			message: req.body.message,
			postcode: req.body.postcode,
			amount: req.body.amount,
			token: req.body.token
		};

		console.log('DONATION', donation);

		controllers.wedding.find('id', id).then(wedding => {

			controllers.stripe.charge(wedding.stripe, donation).then(response => {

				console.log('DONATION', response);

				controllers.wedding.donate(donation).then((ids) => {

					console.log('DONATION', ids);

					res.redirect('/wedding/' + id + '/donate/');

				}, (err) => {

					console.log('ERROR', err);

					req.flash('error', 'Wedding donation failed!')
					res.redirect('/wedding/' + id + '/donate/');
				});
			}, err => {
				console.log('ERROR', err);

				req.flash('error', 'Wedding donation failed!')
				res.redirect('/wedding/' + id + '/donate/');
			});

		}, (err) => {
			res.render('donate', {
				error: "An error occured with your request"
			});
		});

	});





	router.post('/search/:input', (req, res) => {
		var input = req.params.input;
		res.render('wedding_search', {
			input: input
		});
	});

	router.get('/search', (req, res) => {
		res.render('wedding_search', {});
	});



	router.get('/:id', (req, res) => {
		var id = req.params.id;
		controllers.wedding.find('id', id).then((wedding) => {
			console.log(wedding);
			if (wedding) res.render('wedding', wedding);
			else res.render('wedding', {});
		}, (err) => {
			res.render('wedding', {});
		});
	});

	app.use('/wedding', router);

};