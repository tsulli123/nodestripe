/*
 * Login and authentication
 */

module.exports = (express, app, passport, controllers) => {

	var crypto = require('crypto');
	var sourceUrl = 'http://localhost:3000/login/verify/';


	controllers.event.on('login_created', (user) => {
		console.log(user);
		controllers.stripe.create({
			email: user.email,
			ip: user.ip,
			first_name: user.firstname,
			last_name: user.lastname
		}).then(data => {
			controllers.profile.stripe(user.pid, data.id).then(pid => {

			}, err => {
				console.log("USER CREATE ERROR", err);
			});
		}, err => {
			console.log("CREATE STRIPE ERROR", err);
		});
	});


	var router = express.Router();

	router.get('/', (req, res) => {
		res.render('login', {
			error: req.flash('error')
		});
	});

	router.get('/create', (req, res) => {
		res.render('login/login_create', {
			error: req.flash('error')
		});
	});


	router.post('/create', (req, res) => {

		var user = {
			email: req.body.username,
			password: req.body.password,
			firstname: req.body.firstname,
			lastname: req.body.lastname,
			key: crypto.randomBytes(12).toString('hex'),
			ip: req.ip
		};

		controllers.login.create(user).then((ids) => {
			user.id = ids[1];
			user.pid = ids[0];

			console.log('user created', user);

			if (req.isAuthenticated()) req.logout();
			req.login({
				id: user.id,
				username: user.email,
				password: user.password
			}, err => {
				if (err) res.render('login/login_create', {
					error: 'Account couldn\'t be created!'
				});
				else {
					controllers.event.fire('login_created', user);
					res.redirect('/wedding/create');
				}
			});

		}, err => {
			res.render('login/login_create', {
				error: 'Account couldn\'t be created!'
			});
		});

		/*
		controllers.email.send(user.name, sourceUrl + user.key, (error, response) => {
		    console.log(error, response);
		    if (error) {

		    } else {

		    }
		});
		*/

	});


	router.get('/:id/verify/:key', (req, res) => {
		controllers.login.verify(req.params.id, req.params.key).then((verified) => {
			res.render('login/login_email_verify', {});
		}, (err) => {
			res.render('login/login_email_verify', {
				error: 'Your account could not be verified!'
			});
		});

		/*
		controllers.user.findByKey(req.params.key, (err, user) => {
		    if (err || !user) {
		        res.render('login/login_email_verify', {error: 'Your account could not be found!'});
		    } else if (user.verified === 1) {
		        res.render('login/login_email_verify', {error: 'Your account could not be verified!'});
		    } else {
		        controllers.user.verify(user.id, (err, user) => {
		            res.render('login/login_email_verify', {});
		        });
		    }
		});
		*/
	});


	router.get('/verify', (req, res) => {
		res.render('login/login_email_sent', {});
	});

	router.post('/', (req, res, next) => {
		passport.authenticate('local', {
			successRedirect: '/wedding/24',
			failureRedirect: '/login',
			failureFlash: true
		})(req, res, next);
	});

	app.use('/login', router);


	app.get('/logout', (req, res) => {
		req.logout();
		res.redirect('/');
	});

};