module.exports = (express, app, controllers, multer, fs) => {

	var router = express.Router();

    router.get('/:url(*jpg$)', function (req, res) {
        fs.stat('./images/' + req.params.url, function(error, stats) {
            if (error || !stats.isFile()) res.status(404).end();
            else res.sendFile(req.params.url, {root: './images/'});
        });
    });

	app.use('/image', router);

};
