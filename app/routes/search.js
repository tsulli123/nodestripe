module.exports = (express, app, controllers) => {

	var router = express.Router();

	router.get('/', (req, res) => {
		res.render('wedding_search', {});
	});


    router.post('/', (req, res) => {
        var words = req.body.search;
		controllers.search.weddings(words).then((weddings) => {
			res.render('wedding_search', {
				search: words,
				weddings: weddings
			});
		}, (err) => {
            res.render('wedding_search', {
				search: words,
				error: 'Bad request!'
			});
		});
    });

	app.use('/search', router);

};
