
module.exports = (express, app, passport, jwt, controllers) => {

    var router = express.Router();

    function ApiSecurity(req, res, next) {
        var token = req.body.token || req.query.token || req.headers['x-access-token'];
        controllers.login.verifyApiKey(token).then(user => {
            if (user) {
                req.user = user;
                next();
            } else res.status(401).send({ error: 'Unaurthorized' });
        }).catch(err => res.status(401).send({ error: 'Unaurthorized' }));
    }

    /**
     * @api {get} api/get_api_key User login
     * @apiName GetApiKey
     * @apiGroup Api
     *
     * @apiParam {String} username Users email address.
     * @apiParam {String} password Users password.
     *
     * @apiSuccess {String} key User api key.
     * @apiSuccess {Integer} id User id.
     * @apiError {String} error  Error response.
     */
    router.get('/get_api_key', (req, res, next) => {
        controllers.login.apiLogin(req.query.username, req.query.password).then(data => {
            req.token = data.api_token;
            next();
        }).catch(err => res.status(401).send({ error: 'Unaurthorized' }));
    }, (req, res) => {
        res.json({
            token: req.token
        });
    });



    router.get('/get_profile', ApiSecurity, (req, res) => {
        res.json({
            user: req.user
        });
    });

    /**
     * @api {get} api/logout User logout
     * @apiName GetLogout
     * @apiGroup Login
     *
     * @apiSuccess {Object} data Empty data object.
     * @apiError {String} error  Error response.
     */
    router.get('/release_api_key', (req, res) => {
        req.logout();
        res.json({});
    });

    app.use('/api', router);

};
