
INSERT INTO user_profile (first_name, last_name, image_url, dob) VALUES ('Tommi', 'Sulli', 'images\Tommi_Joshua.jpg', '2017-03-03');
INSERT INTO user_profile (first_name, last_name, image_url, dob) VALUES ('Josh', 'Murchie', 'images\Tommi_Joshua.jpg', '2017-03-03');

INSERT INTO access (access_name, access_password, user_profile_id) VALUES ('admin', 'password', 1);
INSERT INTO access (access_name, access_password, user_profile_id) VALUES ('josh', 'password', 2);
INSERT INTO access (access_name, access_password, user_profile_id) VALUES ('tommi', 'password', 1);

INSERT INTO wedding (id, bride_id, groom_id, image_url, event_date) Values (1, 1, 2, 'images\Tommi_Joshua.jpg', '2017-03-04');


INSERT INTO email_login (email, password, profile_id, verify_key) VALUES ('admin', 'password', 1, '1234');

