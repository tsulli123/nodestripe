
/* Create Schema */

CREATE DATABASE IF NOT EXISTS iwishingwell;
USE iwishingwell;


CREATE TABLE IF NOT EXISTS user_profile (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    image_url VARCHAR(255),
    email VARCHAR(255),
    dob DATE
);

CREATE TABLE IF NOT EXISTS wedding (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    bride_id INTEGER,
    groom_id INTEGER,
    stripe_id VARCHAR(59),
    image_url VARCHAR(255),
    event_date DATE,
	CONSTRAINT fk_bride FOREIGN KEY(bride_id) REFERENCES user_profile(id),
    CONSTRAINT fk_groom FOREIGN KEY(groom_id) REFERENCES user_profile(id)
);

CREATE TABLE IF NOT EXISTS wedding_profile (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    wedding_id INTEGER,
    profile_id INTEGER,
    access_level INTEGER,
	CONSTRAINT fk_wedding FOREIGN KEY(wedding_id) REFERENCES wedding(id),
    CONSTRAINT fk_profile FOREIGN KEY(profile_id) REFERENCES user_profile(id)
);

CREATE TABLE IF NOT EXISTS wedding_donation (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    wedding_id INTEGER,
    full_name VARCHAR(255),
    message VARCHAR(255),
    image VARCHAR(255),
    amount FLOAT,
	CONSTRAINT fk_wedding_donation FOREIGN KEY(wedding_id) REFERENCES wedding(id)
);

CREATE TABLE IF NOT EXISTS access (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    access_name VARCHAR(255) NOT NULL,
    access_password VARCHAR(255) NOT NULL,
    access_verified BOOL DEFAULT 0,
    access_verify_key VARCHAR(64) NOT NULL,
	access_date_created DATETIME DEFAULT NOW(),
    user_profile_id INTEGER,
    CONSTRAINT fk_up FOREIGN KEY(user_profile_id) REFERENCES user_profile(id)
);


