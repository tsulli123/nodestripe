
DELETE FROM access;

DROP TABLE IF EXISTS wedding_profile;
DROP TABLE IF EXISTS wedding_donation;
DROP TABLE IF EXISTS wedding;
DROP TABLE IF EXISTS access;
DROP TABLE IF EXISTS user_profile;

DROP SCHEMA IF EXISTS iwishingwell;
