// knex.js migrations config settings.

module.exports = {
	client: 'mysql',
	connection: {
		host: 'localhost',
		port: '3306',
		database: 'iwishingwell',
		user: 'root',
		password: ''
	},
	pool: {
		min: 2,
		max: 10
	},
	migrations: {
		tableName: 'knex_migrations'
	}

};