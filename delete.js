var c = require('./config.json');
var chalk = require('chalk');
var bodyParser = require('body-parser');

var express = require('express');
var app = express();
var http = require('http').Server(app);
var mongoose = require('mongoose');

// Create a new Express application
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());


mongoose.connect('mongodb://localhost/stripeNode');
var database = require('./app/database.js')(mongoose, chalk);

//Database Models
var schemas = {};
var models = {
	mongoose: mongoose,
	Wedding: require('./app/models/Wedding.js')(mongoose, schemas),
};

//Controllers - database functions
var controller = {
	Wedding: require('./app/controllers/wedding.js')(models, logger),
};

controller.Wedding.drop(function () {
	logger.info('Deleted all data for testing');

	//EXIT
	logger.info('Exiting');
	process.exit();
});