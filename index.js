var express = require('express'),
	app = express(),
	http = require('http').Server(app),
	mysql = require('mysql'),
	fs = require('fs'),
	passport = require('passport'),
	LocalStrategy = require('passport-local').Strategy,
	jwt = require('jsonwebtoken'),
	session = require('express-session'),
	MySQLStore = require('express-mysql-session')(session),
	cookieParser = require('cookie-parser'),
	bodyParser = require('body-parser'),
	flash = require('connect-flash'),
	hbs = require('hbs'),
	multer = require('multer'),
	nodemailer = require('nodemailer'),
	stripe = require('stripe')(require('./config.json').apiKeyTest),
	imageData = require('image-data-uri'),
	chalk = require('chalk');

var config = require('./app/config.js');
var sessionStore = new MySQLStore(config.session);
var knex = require('knex')(config.knex);


var controllers = {
	event: require('./app/controllers/events.js')(),
	login: require('./app/controllers/login.js')(passport, knex, LocalStrategy, jwt),
	email: require('./app/controllers/email.js')(nodemailer, config.email),
	image: require('./app/controllers/image.js')(fs, imageData),
	profile: require('./app/controllers/profile.js')(knex),
	wedding: require('./app/controllers/wedding.js')(knex),
	search: require('./app/controllers/search.js')(knex),
	stripe: require('./app/controllers/stripe.js')(stripe)
};

// web resources folder
app.use(express.static(__dirname + '/www'));
// Handlebars - HBS
app.set('view engine', 'html');
app.engine('html', hbs.__express);
hbs.registerPartials(__dirname + '/views/partials');
// middleware config
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(session({
	name: 'iWishingWell',
	key: 'session_cookie_name',
	secret: 'session_cookie_secret',
	store: sessionStore,
	saveUninitialized: true,
	resave: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());


function secure(req, res, next) {
	if (req.isAuthenticated()) {
		next();
	} else {
		res.redirect('/');
	}
}

app.get('/', (req, res) => {
	res.render('index', {});
});


require('./app/routes/login.js')(express, app, passport, controllers);
require('./app/routes/search.js')(express, app, controllers);
require('./app/routes/wedding.js')(express, app, multer, controllers);
require('./app/routes/image.js')(express, app, controllers, multer, fs);
require('./app/routes/api/api.js')(express, app, passport, jwt, controllers);


var g39 = require('./app/g39.js')(chalk).welcome();
var server = require('./app/server.js')(http, 3000, g39.theme);

process.on('SIGTERM', () => {
	sessionStore.close();
	server.close(() => {
		console.log("Finished all requests");
	});
});
