var App = (function () {

	var sendForm = function SendForm($form, data) {
		return $.ajax({
			method: $form.attr('method'),
			url: $form.attr('action'),
			data: data
		});
	};

	return {
		sendForm: sendForm
	};

})();


(function (App) {

	var load = function LoadImage(src, files, callback) {
		if (files && files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$(src).attr('src', e.target.result);
				if (callback) callback(files[0]);
			}
			reader.readAsDataURL(files[0]);
		}
	};

	App.image = {
		load: load
	};

})(App);



(function (App) {

	Stripe.setPublishableKey('pk_test_S8tDVdrs34bNuAQQu86LTLpO');

	var getCardInput = function GetCardInput() {
		return {
			number: $('input[name=number]').val(),
			cvc: $('input[name=cvc]').val(),
			exp: $('input[name=expiry]').val(),
			address_zip: $('input[name=postcode]').val()
		};
	};

	var createCardToken = function CreateCardToken(card, callback) {
		Stripe.card.createToken(card, function (status, response) {
			if (response.error) callback(response.error);
			else callback(false, {
				postcode: $('input[name=postcode]').val(),
				email: $('input[name=email]').val(),
				message: $('input[name=message]').val(),
				amount: $('input[name=amount]').val(),
				token: response.id
			});
		});
	};

	var createBankToken = function CreateBankToken(callback) {
		Stripe.bankAccount.createToken({
			country: "AU",
			currency: "AUD",
			routing_number: $("input[name=bsb]").val(),
			account_number: $("input[name=number]").val(),
			account_holder_name: $("input[name=name]").val(),
			account_holder_type: 'individual'
		}, function (status, response) {
			if (response.error) callback(response.error);
			else callback(false, response.id);
		});
	};


	/*
	    $.get("http://54.252.151.178:3000/iww/createAcc", {
			'image': $('#idImage').attr('src'),
			'email': $('#email').val(),
			'groomFN': $('#groomFN').val(),
			'groomLN': $('#groomLN').val(),
			'brideFN': $('#brideFN').val(),
			'brideLN': $('#brideLN').val(),
			'btok': response.id,
			'year': $('#dobyear').val(),
			'month': $('#dobmonth').val(),
			'day': $('#dobday').val(),
			'add1': $('#add1').val(),
			'city': $('#city').val(),
			'zip': $('#zip').val(),
			'state': $('#state').val(),
		})
		.done(function (data) {
			alert('Your wedding ID is: ' + data.wid)

			$('#create').hide();
			$('#searchBar').show();

			couples.push(data);
			console.log(data);
		});
	*/


	App.stripe = {
		createCardToken: createCardToken,
		createBankToken: createBankToken,
		getCardInput: getCardInput
	};

})(App);


$(function () {

	// api login test
	/*$.ajax({
		method: 'GET',
		url: '/api/get_api_key',
		data: {
			username: 'zyionattiig@yahoo.com.au',
			password: 'Password'
		}
	}).done(function(res) {
		console.log(res.token);

		$.ajax({
			method: 'GET',
			url: '/api/get_profile',
			data: {
				token: res.token
			}
		}).done(function(res) {
			console.log(res);
		});
	});*/
	// end


	$('#update_bank').submit(function (e) {
		e.preventDefault();
		var $form = $(this);
		$form.find('.submit').prop('disabled', true);
		App.stripe.createBankToken(function (err, id) {
			if (err) console.log('Error creating bank token!');
			else {
				$('#idcode').val(id);
				App.sendForm($form, $form.serializeArray()).done(function (res) {
					console.log('Success sending bank token!');
				}).fail(function (err) {
					console.log('Error sending bank token!');
				});
			}
			$form.find('.submit').prop('disabled', false);
		});
	});

	$('#form-donate').submit(function (e) {
		e.preventDefault();
		var $form = $(this);
		$form.find('.submit').prop('disabled', true);
		App.stripe.createCardToken(App.stripe.getCardInput(), function (err, data) {
			if (err) console.log('Error creating card token!', err);
			else {
				App.sendForm($form, data).done(function (res) {
					console.log('Success sending card token!');
				}).fail(function (err) {
					console.log('Error sending card token!');
				});
			}
			$form.find('.submit').prop('disabled', false);
		});
	});

	// $('#image-input').change(function(e) {
	// 	App.image.load('#image-input-view', this.files);
	// });

	$('#image-input-view')
		.on('dragover', function (e) {
			e.preventDefault();
			event.stopPropagation();
			$(this).addClass('dragging');
		})
		.on('dragleave', function (e) {
			e.preventDefault();
			event.stopPropagation();
			$(this).removeClass('dragging');
		})
		.on('drop', function (e) {
			e.preventDefault();
			event.stopPropagation();
			if (e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files) {
				App.image.load('#image-input-view', e.originalEvent.dataTransfer.files, function (file) {
					//$('#image-input').val(file.name);
				});
			}
		});

});