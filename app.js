//imports

/***************************************************************************

 ██████╗ ██████╗  █████╗     ██╗   ██╗███████╗███╗   ██╗████████╗██╗   ██╗██████╗ ███████╗███████╗
██╔════╝ ╚════██╗██╔══██╗    ██║   ██║██╔════╝████╗  ██║╚══██╔══╝██║   ██║██╔══██╗██╔════╝██╔════╝
██║  ███╗ █████╔╝╚██████║    ██║   ██║█████╗  ██╔██╗ ██║   ██║   ██║   ██║██████╔╝█████╗  ███████╗
██║   ██║ ╚═══██╗ ╚═══██║    ╚██╗ ██╔╝██╔══╝  ██║╚██╗██║   ██║   ██║   ██║██╔══██╗██╔══╝  ╚════██║
╚██████╔╝██████╔╝ █████╔╝     ╚████╔╝ ███████╗██║ ╚████║   ██║   ╚██████╔╝██║  ██║███████╗███████║
 ╚═════╝ ╚═════╝  ╚════╝       ╚═══╝  ╚══════╝╚═╝  ╚═══╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚══════╝╚══════╝
					
	Copyright © 2016 G39 Ventures. All Rights Reserved.
	

****************************************************************************/

var c = require('./config.json');
var chalk = require('chalk');
var bodyParser = require('body-parser');

var express = require('express');
var app = express();
var http = require('http').Server(app);
var mongoose = require('mongoose');
var stripe = require('stripe')(c.apiKeyTest);
var logger = require('winston');
var multer = require('multer');
var fs = require('fs');
var imageData = require('image-data-uri');
var theme = chalk.bold.blue;
//var io = require('socket.io-compress')(http);

// Create a new Express application
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});


mongoose.connect('mongodb://localhost/stripeNode');
var database = require('./app/database.js')(mongoose, chalk);

//Database Models
var schemas = {};
var models = {
	mongoose: mongoose,
	Wedding: require('./app/models/Wedding.js')(mongoose, schemas),
};

//Controllers - database functions
var controller = {
	Wedding: require('./app/controllers/wedding.js')(models, logger),
};


require('./app/routes.js')(app, stripe, controller, fs, imageData);

//Server connection
var server = require('./app/server.js')(http, 3001, theme);